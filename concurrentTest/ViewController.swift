//
//  ViewController.swift
//  concurrentTest
//
//  Created by SungJae Lee on 2016. 12. 1..
//  Copyright © 2016년 SungJae Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .red
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.addSubview(imageView)
        imageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        let queue = OperationQueue()
        
        let loadOperation = AsyncBlockOperation { (op) in
            
            let url: URL = URL(string: "http://image.shutterstock.com/display_pic_with_logo/2023169/231023848/stock-photo-dog-playing-outside-smiles-231023848.jpg")!
            
            let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
                guard error == nil , let unWrappedData = data else { print(error!); return }
                
                    DispatchQueue.main.async {
                        self.imageView.image = UIImage(data:unWrappedData)
                    }
                
                    //print(String(data: loadedData!, encoding: .utf8)!)
                    op.complete()
                }
            task.resume()

            }
        
        let showOperation = BlockOperation {
          
        }
    
        showOperation.addDependency(loadOperation)
      
        queue.addOperations([loadOperation,showOperation], waitUntilFinished: false)
    
    }


}

