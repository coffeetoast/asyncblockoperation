//
//  AsyncBlockOperation.swift
//  concurrentTest
//
//  Created by SungJae Lee on 2016. 12. 1..
//  Copyright © 2016년 SungJae Lee. All rights reserved.
//

import UIKit

class AsyncBlockOperation: Operation
{
    enum State: String {
        case Ready
        case Executing
        case Finished
        
        fileprivate var keyPath: String {
            print("is" + rawValue)
            return "is" + rawValue
        }
    }
    
    var state = State.Ready {
        willSet {
            willChangeValue(forKey: newValue.keyPath)
            willChangeValue(forKey: state.keyPath)
        }
        
        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: state.keyPath)
        }
        
    }
    
    typealias AsyncBlock = (AsyncBlockOperation) -> ()
    
    let asyncBlock: AsyncBlock?
    
    
    init(asyncBlock: @escaping AsyncBlock) {
        self.asyncBlock = asyncBlock
    }
}

extension AsyncBlockOperation
{
    override var isReady: Bool {
        return super.isReady && state == .Ready
    }
    
    override var isExecuting: Bool {
        return state == .Executing
    }
    
    override var isFinished: Bool {
        return state == .Finished
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    
    override func start() {
        guard !isCancelled else {
            state = .Finished
            return
        }
        state = .Executing
        
        if let executingBlock = self.asyncBlock {
            executingBlock(self)
        } else {
            complete()
        }
        
    }
    
    func complete () {
        willChangeValue(forKey: "isExecuting")
        willChangeValue(forKey: "isFinished")
        state = .Finished
        didChangeValue(forKey: "isExecuting")
        didChangeValue(forKey: "isFinished")
    }
    
    override func cancel() {
        state = .Finished
    }
}

extension OperationQueue {
    
    func addOperationWithAsyncBlock(block: AsyncBlockOperation) {
        self.addOperation(block)
    }
}
