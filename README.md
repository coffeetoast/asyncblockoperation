# README #

main()을 override 하는 Operation은 sync로 돌아가기 때문에 
NSOperation 사용시 Concurrent 로 돌려도 async 함수들을 operation 안에서 사용시 문제가 생긴다. 
callback이 돌아오기 전에 operation 이 queue에서 살아지기 때문이다.

따라서 start()를 override 하여 수동으로 state를 감시해 ConcurrentOperation을 수동으로 dirty hand 해야한다.  
가볍게 BlockOperation을 자주 사용하게 되는데 async func 들도 사용 가능한 AsyncBlockOperation을 만들어 보았다. 

수동으로 이렇게 만들기 귀찮으면 DispatchSemaphore를 이용해서 wait 했다가 calback 돌아오면 signal 주는 
방법도 있다. :) 

Swift 3 , Xcode 8.1 호환 입니다. 
사용법) 

queue.addOperation(operation)
```
#!Swift
let operation = AsyncBlockOperation { op in
    doSomeAsyncTaskWithCompletionBlock {
        op.complete() // complete operation
    }
}

```


inspired by https://github.com/devxoul/AsyncBlockOperation